
class Solution:
    n_instances = 0
    def __init__(self):

        #initialize
        self.stock_costs = float(0.0)
        self.routingCosts = float(0.0)
        self.totalCosts = float(0.0)
        self.refinedRoutingCosts = float(0.0)

        # update
        Solution.n_instances += 1
        self.id = Solution.n_instances
