
from outputs import Outputs

class MultiStartTester:
    inputFolder = "inputs"
    outputFolder = "outputs"
    testFolder = "tests"
    fileNameTest = "test2Run2.txt"
    sufixFileNodes = ".txt"
    sufixFileVehicules = "_input_vehicles.txt"
    sufixFileOutput = "_outputs.txt"

    def main_fun(self, args):
        out_list = [Outputs()] #list Outputs instances defined in Outputs.py
        out_list_dist = [Outputs()] # idem
        sol_to_print = [[Outputs()],[Outputs()]]
